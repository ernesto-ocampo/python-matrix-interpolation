import os
from math import isnan

import pytest

from matrixinterp.interpolate_matrix import (
    load_matrix_from_file
)
from matrixinterp.tests.test_utils import write_matrix


@pytest.fixture
def path_in(tmpdir):
    return os.path.join(str(tmpdir), 'in.csv')


def test_load_matrix_func():
    assert callable(load_matrix_from_file)


def test_load_matrix_invalid_path():
    with pytest.raises(IOError):
        load_matrix_from_file('somepath.csv')


def test_load_matrix_empty(path_in):
    open(path_in, 'w').close()
    m = load_matrix_from_file(path_in)
    assert m.size == 0


def test_load_matrix_values(path_in):
    write_matrix([
        [1.1, 2.2],
        [3.3, 4.4],
        [5.5, 6]
    ], path_in)
    m = load_matrix_from_file(path_in)
    assert m.item((0, 0)) == 1.1
    assert m.item((0, 1)) == 2.2
    assert m.item((1, 0)) == 3.3
    assert m.item((1, 1)) == 4.4
    assert m.item((2, 0)) == 5.5
    assert m.item((2, 1)) == 6.0


def test_load_matrix_with_nan(path_in):
    write_matrix([
        [1.1, 2.2],
        [3.3, 'nan']
    ], path_in)
    m = load_matrix_from_file(path_in)
    assert m.item((0, 0)) == 1.1
    assert m.item((0, 1)) == 2.2
    assert m.item((1, 0)) == 3.3
    assert isnan(m.item((1, 1)))


@pytest.mark.parametrize('matrix_value', [
    [['1.1;2.2']],          # bad separator
    [[1.1, 'hello']],       # non-numeric
    [[1.1, 2.2], [3.3]],    # differing row lengths
])
def test_load_matrix_bad_format(path_in, matrix_value):
    write_matrix(matrix_value, path_in)
    with pytest.raises(ValueError):
        load_matrix_from_file(path_in)
