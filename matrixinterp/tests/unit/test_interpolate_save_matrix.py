import os

import numpy
import pytest


from matrixinterp.interpolate_matrix import (
    load_matrix_from_file,
    save_matrix_to_file
)
from matrixinterp.tests.unit.test_interpolate_load_matrix import (
    path_in,
    write_matrix
)


@pytest.fixture
def path_out(tmpdir):
    return os.path.join(str(tmpdir), 'out.csv')


def test_save_matrix_func():
    assert callable(save_matrix_to_file)


@pytest.mark.parametrize('data', [
    [[]],
    [[1.0]],
    [[1.1, 2.2], [3.3, 4.4]]
])
def test_write_load_compare(data, path_in, path_out):
    write_matrix(data, path_in)
    original = load_matrix_from_file(path_in)
    save_matrix_to_file(original, path_out)
    loaded = load_matrix_from_file(path_out)
    assert (original == loaded).all()


def test_saved_precision_6_digits(path_out):
    high_precision_matrix = numpy.matrix([[65.33655300000001]])
    save_matrix_to_file(high_precision_matrix, path_out)
    loaded = load_matrix_from_file(path_out)
    assert loaded.item(0, 0) == 65.336553  # lower precision
