import numpy
import pytest

from matrixinterp.interpolators import AdvancedInterpolator


@pytest.fixture
def interpolator(request):
    yield AdvancedInterpolator()


@pytest.mark.parametrize('data', [
    [1.1,          float('nan'), float('nan')],
    [float('nan'), 1.1,          float('nan')],
    [float('nan'), float('nan'), 1.1]
])
def test_horizontal_one_value(interpolator, data):
    matrix = numpy.matrix(data)
    interpolator.interpolate(matrix)
    assert (matrix == numpy.matrix([1.1, 1.1, 1.1])).all()


def test_1d_intervals(interpolator):
    data = [1.0, float('nan'), float('nan'),
            4.0, float('nan'), 8.0, float('nan')]
    expected = [1.0, 2.0, 3.0, 4.0, 6.0, 8.0, 8.0]
    for i in range(2):
        matrix = numpy.rot90(numpy.matrix(data), i)
        interpolator.interpolate(matrix)
        assert (matrix == numpy.rot90(numpy.matrix(expected), i)).all()


def test_competing_interpolations(interpolator):
    """An interpolated value should not affect further interpolations"""
    data = [
        [1.0, float('nan'), 3.0],
        [4.0, float('nan'), 6.0]
    ]
    expected = [
        [1.0, 2.0, 3.0],
        [4.0, 5.0, 6.0]
    ]
    for i in range(2):
        matrix = numpy.rot90(numpy.matrix(data), i)
        interpolator.interpolate(matrix)
        assert (matrix == numpy.rot90(numpy.matrix(expected), i)).all()