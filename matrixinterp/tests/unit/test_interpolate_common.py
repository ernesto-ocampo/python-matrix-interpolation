import os

import numpy
import pytest


from matrixinterp.interpolate_matrix import (
    load_matrix_from_file,
)
from matrixinterp.interpolators import (
    InterpolationError,
    NonContiguousInterpolator,
    AdvancedInterpolator,
    IterativeInterpolator
)


@pytest.fixture(params=[NonContiguousInterpolator,
                        AdvancedInterpolator,
                        IterativeInterpolator])
def interpolator(request):
    yield request.param()


def test_returns_none(interpolator):
    assert interpolator.interpolate(numpy.matrix([1, 2, 3])) is None


def test_empty_matrix(interpolator, tmpdir):
    path_in = os.path.join(str(tmpdir), 'in.csv')
    open(path_in, 'w').close()
    matrix = load_matrix_from_file(path_in)
    interpolator.interpolate(matrix)
    assert (matrix == load_matrix_from_file(path_in)).all()


def test_one_row_matrix(interpolator, tmpdir):
    path_in = os.path.join(str(tmpdir), 'in.csv')
    with open(path_in, 'w') as in_file:
        in_file.write('1.1,2,2,3,3\n')
    matrix = load_matrix_from_file(path_in)
    interpolator.interpolate(matrix)
    assert (matrix == load_matrix_from_file(path_in)).all()


@pytest.mark.parametrize('data', [
    [[1.1, 2], [3.3, 4]],
    [1.0, 2.1],
    [1.0]
])
def test_no_missing_values(interpolator, data):
    matrix = numpy.matrix(data)
    interpolator.interpolate(matrix)
    assert (matrix == numpy.matrix(data)).all()


def test_only_nan(interpolator):
    matrix = numpy.matrix([float('nan')])
    with pytest.raises(InterpolationError):
        interpolator.interpolate(matrix)


@pytest.mark.parametrize('data,expected', [
    ([1.1, float('nan')], [1.1, 1.1]),
    ([float('nan'), 1.1], [1.1, 1.1]),
    ([[1.1], [float('nan')]], [[1.1], [1.1]]),
    ([[float('nan')], [1.1]], [[1.1], [1.1]]),
])
def test_interpolate_one_neighbour(interpolator, data, expected):
    matrix = numpy.matrix(data)
    interpolator.interpolate(matrix)
    assert (matrix == numpy.matrix(expected)).all()


def test_interpolate_two_neighbours(interpolator):
    data = [[float('nan'), 1.5, 2.0],
            [3.5,          4.0, 5.0],
            [6.0,          7.0, 8.0]]

    expected = [[2.5, 1.5, 2.0],
                [3.5, 4.0, 5.0],
                [6.0, 7.0, 8.0]]

    for i in range(4):
        matrix = numpy.rot90(numpy.matrix(data), i)
        interpolator.interpolate(matrix)
        assert (matrix == numpy.rot90(numpy.matrix(expected), i)).all()


def test_interpolate_three_neighbours(interpolator):
    data = [[1.5, float('nan'), 2.0],
            [3.5, 4.0,          5.0],
            [6.0, 7.0,          8.0]]

    expected = [[1.5, 2.5, 2.0],
                [3.5, 4.0, 5.0],
                [6.0, 7.0, 8.0]]

    for i in range(4):
        matrix = numpy.rot90(numpy.matrix(data), i)
        interpolator.interpolate(matrix)
        assert (matrix == numpy.rot90(numpy.matrix(expected), i)).all()


def test_interpolate_four_neighbours(interpolator):
    data = [[1.5, 4.0,          2.0],
            [3.5, float('nan'), 5.0],
            [6.0, 7.0,          8.0]]

    expected = [[1.5, 4.0,   2.0],
                [3.5, 4.875, 5.0],
                [6.0, 7.0,   8.0]]

    for i in range(4):
        matrix = numpy.rot90(numpy.matrix(data), i)
        interpolator.interpolate(matrix)
        assert (matrix == numpy.rot90(numpy.matrix(expected), i)).all()


def test_all_nan(interpolator):
    matrix = numpy.ones((4, 6), dtype=float)
    matrix.fill(float('nan'))
    with pytest.raises(InterpolationError):
        interpolator.interpolate(matrix)
