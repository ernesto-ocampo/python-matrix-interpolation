
def read_matrix_from_lines(lines):
    """Read a string iterable into a list of lists"""
    return [
        [float(part) for part in line.split(',')]
        for line in lines
        if line
    ]

def read_matrix(path):
    """Read a matrix file into a list of lists"""
    with open(path, 'r') as matrix_file:
        return read_matrix_from_lines(matrix_file)


def write_matrix(matrix, path):
    """Writes a matrix as a list of lists to path"""
    with open(path, 'w') as out_file:
        out_file.writelines(
            ",".join(str(value) for value in row) + '\n'
            for row in matrix
        )
