import os

from lettuce import before, step, world

from matrixinterp.tests.test_utils import read_matrix


@before.all
def init_args():
    world.tool_args = [None, None]


def expected_path(name):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        'expected', name)


def input_path(name):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        'inputs', name)


@step("file (.+) does not exist")
def step_impl(step, path):
    assert not os.path.exists(os.path.join(world.tmp_path, path))


@step("I get output as in (.+)")
def step_impl(step, expected_name):
    actual = read_matrix(world.tool_args[1])
    expected = read_matrix(expected_path(expected_name))
    assert actual == expected, \
        "\nGot      {},\nexpected {}".format(actual, expected)


@step("The test input (?P<infile>.+)")
def step_impl(step, infile):
    world.tool_args[0] = input_path(infile)
    if '-cont' in world.tool_args:
        world.tool_args.remove('-cont')
    if '-iter' in world.tool_args:
        world.tool_args.remove('-iter')


@step("the output path (.+)")
def step_impl(step, outfile):
    world.tool_args[1] = os.path.join(world.tmp_path, outfile)
