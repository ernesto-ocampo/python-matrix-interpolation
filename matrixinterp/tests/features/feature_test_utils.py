from subprocess import Popen, PIPE
import os
import shutil
import sys
import tempfile

from lettuce import before, after, step, world

import matrixinterp


@before.all
def create_tmp_dir():
    world.tmp_path = tempfile.mkdtemp()


@after.all
def clean_tmp_dir(total):
    shutil.rmtree(world.tmp_path)


def run_tool(*args):
    """Runs the tool and returns an (exit_code, stdout, stderr) tuple"""
    process = Popen([sys.executable, '-m', matrixinterp.__name__]
                    + list(args), stdout=PIPE, stderr=PIPE)
    out, err = process.communicate(input=None)
    exit_code = process.returncode
    return exit_code, out, err


@step('I invoke the tool')
def invoke_tool(step):
    world.exit_code, world.tool_out, world.tool_err = \
        run_tool(*world.tool_args)


@step('I get exit code (\d+)')
def i_get_exit_code(step, expected):
    assert world.exit_code == int(expected), \
        'Got {}, expected {}, stderr is {}'.format(
            world.exit_code, expected, world.tool_err)


@step("I specify arguments (.+)")
def step_impl(step, args):
    world.tool_args = [os.path.join(world.tmp_path, arg)
                       for arg in args.split(' ')]
