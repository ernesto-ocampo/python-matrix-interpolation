Feature: Basic interpolation
    In order to interpolate single matrix values
    As users of the interpolation tool
    We'll implement a tool to interpolate single single non-contiguous missing values in matrices.
    Input matrices should be specified using CSV format, and missing values should be denoted as
    'nan'.

    Scenario Outline: Bad input matrix format - no output
        Given The test input <infile>
            And the output path outfile
        When I invoke the tool
        Then I get exit code 4
            And file outfile does not exist

        Examples:
            | infile                            |
            | bad-format-bad-separator.csv      |
            | bad-format-bad-missing-format.csv |
            | bad-format-differing-lengths.csv  |
            | bad-format-invalid-number.csv     |
            | bad-format-letters.csv            |


    Scenario Outline: Bad matrix data
        Given The test input <infile>
            And the output path outfile
        When I invoke the tool
        Then I get exit code 5
            And file outfile does not exist

        Examples:
            | infile                                   |
            | bad-in-contiguous-missing-horizontal.csv |
            | bad-in-contiguous-missing-vertical.csv   |
            | bad-in-one-value-and-missing.csv         |


    Scenario Outline: Correct input, expected output
        Given The test input <infile>
            And the output path output.csv
        When I invoke the tool
        Then I get exit code 0
        And I get output as in <expected>

        Examples:
            | infile                   | expected                  |
            | good-in-empty.csv        | good-out-empty.csv        |
            | good-in-no-missing.csv   | good-out-no-missing.csv   |
            | good-in-one-value.csv    | good-out-one-value.csv    |
            | good-in-with-missing.csv | good-out-interpolated.csv |
