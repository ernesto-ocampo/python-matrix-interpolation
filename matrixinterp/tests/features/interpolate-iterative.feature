Feature: Iterative Interpolation
    In order to interpolate contiguous missing matrix values
    As users of the interpolation tool
    We'll implement an iterative interpolation feature that is a g
    generalisation of Advanced Interpolation and that can handle
    all matrices that have at least 1 concrete value - even if there
    are full missing rows and columns.

    Iterative interpolation should apply Advanced Interpolation
    iteratively, until there are no changes. Interpolated values
    will eventually propagate and fill the entire matrix.

    Scenario: Iterative interpolation
      Given The input matrix:
        """
        1,nan,nan
        nan,nan,nan
        3,nan,nan
        """
        And the output path output.csv
        And I use the iterative option
      When I invoke the tool
      Then I get exit code 0
        And I get this output:
          """
          1,1,1
          2,2,2
          3,3,3
          """

    Scenario Outline: Correct input, expected output
        Given The test input <infile>
          And the output path output.csv
          And I use the iterative option
        When I invoke the tool
        Then I get exit code 0
        And I get output as in <expected>

        Examples:
            | infile                   | expected                  |
            | good-in-empty.csv        | good-out-empty.csv        |
            | good-in-no-missing.csv   | good-out-no-missing.csv   |
            | good-in-one-value.csv    | good-out-one-value.csv    |
            | good-in-with-missing.csv | good-out-interpolated.csv |
