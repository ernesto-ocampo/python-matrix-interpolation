Feature: Advanced Interpolation
    In order to interpolate contiguous missing matrix values
    As users of the interpolation tool
    We'll implement an interpolation feature that can handle contiguous
    missing values. This is a generalisation of basic interpolation.
    An interpolated can have up to two components: horizontal interpolation
    and vertical interpolation.

    Horizontal interpolation: linear interpolation between a lower and an
    upper boundary. For example: 1 nan nan 4 would be horizontally
    interpolated as 1 2 3 4. When only one value is available, horizontal
    interpolation takes that value, for example 1 nan nan nan is interpolated
    as 1 1 1 1. Vertical interpolation is analogous. When both horizontal
    and vertical interpolations are possible, the resulting value is the
    simple average of both.

    An interpolated value should NOT be used for further interpolations,
    i.e. the order in which interpolated values are calculated should
    not affect the result.

    If a missing value lies in a row and column made of only missing values,
    then interpolation is not possible.

    Scenario: Horizontal interpolation
      Given The input matrix:
        """
        1,nan,nan,4
        """
        And the output path output.csv
        And I use the contiguous option
      When I invoke the tool
      Then I get exit code 0
        And I get this output:
          """
          1,2,3,4
          """

    Scenario: Horizontal one-value interpolation
      Given The input matrix:
        """
        nan,1,nan,nan
        """
        And the output path output.csv
        And I use the contiguous option
      When I invoke the tool
      Then I get exit code 0
        And I get this output:
          """
          1,1,1,1
          """

    Scenario: Vertical interpolation
      Given The input matrix:
        """
        1
        nan
        nan
        4
        """
        And the output path output.csv
        And I use the contiguous option
      When I invoke the tool
      Then I get exit code 0
        And I get this output:
          """
          1
          2
          3
          4
          """

    Scenario: Horizontal and vertical interpolation
      Given The input matrix:
        """
        1.0,2.0,3.0,4.0
        5.0,nan,nan,8.0
        9.0,4.0,5.0,6.0
        """
        And the output path output.csv
        And I use the contiguous option
      When I invoke the tool
      Then I get exit code 0
        And I get this output:
          """
          1.0,2.0,3.0,4.0
          5.0,4.5,5.5,8.0
          9.0,4.0,5.0,6.0
          """

    Scenario: All nan row and column
      Given The input matrix:
        """
        1.0,nan,3.0,4.0
        nan,nan,nan,nan
        9.0,nan,2.0,3.0
        """
        And the output path output2.csv
        And I use the contiguous option
      When I invoke the tool
      Then I get exit code 5
        And file output2.csv does not exist

    Scenario Outline: Correct input, expected output
        Given The test input <infile>
          And the output path output.csv
          And I use the contiguous option
        When I invoke the tool
        Then I get exit code 0
        And I get output as in <expected>

        Examples:
            | infile                   | expected                  |
            | good-in-empty.csv        | good-out-empty.csv        |
            | good-in-no-missing.csv   | good-out-no-missing.csv   |
            | good-in-one-value.csv    | good-out-one-value.csv    |
            | good-in-with-missing.csv | good-out-interpolated.csv |
