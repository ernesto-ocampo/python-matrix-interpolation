"""
Performs interpolation.

Exit codes:
    0: Ok, output generated.
    1: General failure.
    2: Bad arguments
    3: Cannot read input or bad path, or output path is an existing directory
    4: Bad input matrix format
    5: Cannot interpolate: data problem, e.g. insufficient present values,
        contiguous values.
"""

import argparse
import os
import sys

import numpy

from interpolators import (
    NonContiguousInterpolator,
    InterpolationError,
    AdvancedInterpolator,
    IterativeInterpolator
)


def load_matrix_from_file(csv_path):
    """Loads a matrix from a CSV file.

    The file may contain float values: either concrete values or 'nan'.
    :param csv_path: path to input CSV file
    :return: a numpy matrix: numpy.ndarray
    """
    return numpy.loadtxt(csv_path, delimiter=',', ndmin=2)


def save_matrix_to_file(matrix, csv_path):
    """Saves a matrix to file in CSV format.

    Float values are saved with 6 digits precision.
    :param matrix: a numpy matrix
    :type matrix: numpy.ndarray
    :param csv_path: path of file to write to
    :return: None
    """
    numpy.savetxt(csv_path, matrix, fmt='%.7f', delimiter=',', newline='\n')


def _get_interpolator(args):
    """Gets interpolator according to args"""
    if args.cont:
        return AdvancedInterpolator()
    elif args.iter:
        return IterativeInterpolator()
    else:
        return NonContiguousInterpolator()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', help='Matrix input file')
    parser.add_argument('outfile', help='Matrix output file')
    parser.add_argument('-cont', action='store_true', default=False, help='Allow contiguous missing values')
    parser.add_argument('-iter', action='store_true', default=False, help='Allow contiguous missing values')
    args = parser.parse_args()

    if os.path.isdir(args.outfile):
        sys.stderr.write('Output path is a directory')
        sys.exit(3)

    try:
        matrix = load_matrix_from_file(args.infile)
    except IOError as e:
        sys.stderr.write('Cannot read input file: {}'.format(e))
        sys.exit(3)
    except ValueError as e:
        sys.stderr.write('Bad input data format: {}'.format(e))
        sys.exit(4)

    try:
        _get_interpolator(args).interpolate(matrix)
    except InterpolationError as e:
        sys.stderr.write('Cannot interpolate: {}'.format(e))
        sys.exit(5)

    save_matrix_to_file(matrix, args.outfile)


if __name__ == '__main__':
    main()
