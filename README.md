Matrix Interpolation
====================

[![build status](https://gitlab.com/ernesto-ocampo/python-matrix-interpolation/badges/master/build.svg)](https://gitlab.com/ernesto-ocampo/python-matrix-interpolation/pipelines)

# Install and run tests

```
tar -xzf python-matrix-interpolation.tar.gz

or

git clone https://gitlab.com/ernesto-ocampo/python-matrix-interpolation

cd python-matrix-interpolation
virtualenv venv
source venv/bin/activate
pip install -e .
pytest --pyargs matrixinterp
lettuce ./matrixinterp/tests/features/
```


# Task Completion notes

Lettuce was used for automating acceptance testing. This is the first time
I used this tool so I learned a few things along the way and the last tests
may look a bit better than the first. Please find the feature specifications in:

- tool_interface.feature
- interpolate.feature
- interpolate-with-contiguous.feature
- interpolate-iterative.feature

The rest of the proceess follows intuitively using a TDD approach. Features
are written first, then unit tests, then code. This defines larger and smaller
cycles.

# Notes on design decisions

### Tool interface is file-based

We could have used stdin and stdout for input and output of matrices to the 
tool. However, this conveys the idea that the matrices flow through pipes,
not being ever fully accumulated in memory. Achieving such a thing is 
non-trivial, and it is not clear whether this is of any value to our users. 
For this reason, the input/output interface for this tool will be file-based.

### Acceptance and unit tests

Our users (the business) required a "*tool to read in a two-dimensional matrix
with non-adjacent missing values and output a matrix with no missing values*".

The explicit requirement is that there be such a tool. There is always an
implicit requirement that the SW engineering team stays productive and 
efficient. This implicit requirement guides many of our design decisions
to write maintainable, reusable, extensible, readable, testable code. 
For this reason, reusable functions have been constructed, rather than a 
monolithic main script.

However, acceptance tests only care about the explicit business 
requirement; they assert that the tool satisfies (and continues to satisfy)
the requirement, and do not know about the internal details of how
the tool achieves this. The acceptance tests are (part of) the requirement 
specification. Acceptance tests are written by a collaboration of developers,
tests, and stakeholders, for the business.

Unit test will indeed have knowledge on all the internals of the system. Unit
tests are written by developers to achieve a productive software engineering
work discipline (which is in the best interests of the business). In TDD,
unit tests assert everything that is to be assumed of the code - e.g. if 
it is to be assumed that a function returns a float, some test should assert 
that.

### About precision digits in output

In the example data `interpolated_test_data.csv`, all values have 6 digits 
precision, except for one value, which has 7 digits. In a situation like
this, developers and stakeholders must work to agree on one format. I have
assumed agreement on a 7-digit format. Therefore, all outputs generated
by the tool have 7-digit precision.

# Task

To create a production quality python tool to read in a two-dimensional matrix with non-adjacent 
missing values and output a matrix with no missing values. Example input and output data files are
provided in the 'example_data' directory.

- The missing values should be interpolated as the average of all neighbouring non-diagonal values.
- Input is a comma separated matrix of values with missing values specified as 'nan'.
- Output is required to be a comma separated matrix of values.
- Non-interpolated values should not change.

The tool should be developed with acceptance and unit tests that would aid further collaborative 
development. The purpose of this task is for you to showcase your approach to software development. 
Please do not spend more than three hours on this task.

# Extra

If you complete the task in less than three hours, you may consider the following features in the
remaining time:
- Handle adjacent missing values. The choice of how to interpolate in this case is left to you.
- Exit gracefully with malformed input specifications and data.
